<?php

/*
Plugin Name: Simplongin
Author: Simplon
Version: 1.0.0
Description: Un plugin fait à Simplon qui ajoute un widget
*/


add_action('widgets_init', 'load_widget');
function load_widget() {
    include 'ModalWidget.php';
    register_widget('ModalWidget');
}

add_action('wp_enqueue_scripts', 'load_assets');
function load_assets() {
    wp_enqueue_script('simplongin_script', plugins_url('js/script.js', __FILE__), [], false, true);
    wp_enqueue_style('simplongin_style', plugins_url('css/style.css', __FILE__));

}


//Optionnel (ajout d'un menu dans l'admin du wordpress)
add_action('admin_menu','add_plugin_menu');
function add_plugin_menu() {
    add_menu_page("Mon Plugin", "Mon Plugin Settings", "manage_options", "simplon_menu", "test_init");
}
function test_init(){
    echo "<h1>Simplon Plugin Settings</h1>";
}
//Fin optionnel