let buttons = document.querySelectorAll('.btn-modal');
let modals = document.querySelectorAll('.modal-bg');

for (let button of buttons) {
    button.addEventListener('click', function () {
        let id = this.getAttribute('data-target');
        let target = document.querySelector('.modal-bg[data-id="' + id + '"]');
        target.style.display = 'block';
    });
}

for (let modal of modals) {
    modal.addEventListener('click', function () {
        modal.style.display = 'none';
    });
}
