<?php


class ModalWidget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct("modal_widget", "Modal Widget", [
            "description" => "Un widget pour afficher une modale",
            "classname" => "modal-widget"
        ]);
    }

    public function widget($args, $instance)
    {
        $id = uniqid();
        ?>
    <div class="modal-bg" data-id="<?php echo $id ?>" >
        <div class="modal">
            <?php echo $instance['content'] ?>
        </div>
    </div>
    <button class="btn-modal" data-target="<?php echo $id ?>">
        Open Modal
    </button>
    <?php

    }

    public function form($instance)
    { 
        ?>
        <label for="<?php echo $this->get_field_id('content'); ?>">
            Content    
        </label>
        <textarea name="<?php echo $this->get_field_name('content'); ?>" 
        id="<?php echo $this->get_field_id('content'); ?>"><?php echo $instance['content'] ?></textarea>
        <?php
    }

    public function update($new_instance, $old_instance)
    { 
        $instance = [];
        $instance['content'] = $new_instance['content'];

        return $instance;

    }
}
